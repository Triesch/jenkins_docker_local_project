#Node image als base
FROM node:7-onbuild

# set maintainer
LABEL maintainer "jotriesch@gmail.com"

# set health check
HEALTHCHECK --interval=5s \
            --timeout=5s \
            CMD curl -f http://127.0.0.1:8000 || exit 1

#port to expose
EXPOSE 8000